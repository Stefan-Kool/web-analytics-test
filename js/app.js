// Multistep form

let currentTab = 0; // Current tab is set to be the first tab (0)
let eventTab = currentTab;
let eventSuccess = 'failed';

showTab(currentTab); // Display the current tab

function showTab(n) {
    // This function will display the specified tab of the form ...
    const x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // ... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
        document.getElementById("nextBtn").style.display = "none";
        document.getElementById("submitBtn").style.display = "inline";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
        document.getElementById("nextBtn").style.display = "inline";
        document.getElementById("submitBtn").style.display = "none";
    }
    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n)
}

function prevTab(n) {
    // This function will figure out which tab to display
    const x = document.getElementsByClassName("tab");
    
    // Hide the current tab:
    x[currentTab].style.display = "none";
    
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    
    // Display the correct tab:
    showTab(currentTab);
}

function nextTab(n) {
    // This function will figure out which tab to display
    const x = document.getElementsByClassName("tab");

    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()){
        eventSuccess = 'failed';
        captureEvent();
        return false;
    }

    // Hide the current tab:
    x[currentTab].style.display = "none";

    // Set success
    eventSuccess = 'success';

    // Capture event
    captureEvent();

    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;

    // if you have reached the end of the form... :
    if (currentTab >= x.length) {
        //...the form gets submitted:
        // document.getElementById("regForm").submit();
        console.log('Finished!!!');
        return false;
    }

    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function validateForm() {
    // This function deals with validation of the form fields
    let x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");

    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false:
            valid = false;
            // If invalid set valid value SK
            stepValid = false;
        }
    }

    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }

    return valid; // return the valid status
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    let i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }

    //... and adds the "active" class to the current step:
    x[n].className += " active";
}

// Firebase configuration
var config = {
    apiKey: "AIzaSyD6mccabAS8z6DKx0lKDcKzjOLjpmO_P_c",
    authDomain: "webanalyticstest-358ec.firebaseapp.com",
    databaseURL: "https://webanalyticstest-358ec.firebaseio.com",
    projectId: "webanalyticstest-358ec",
    storageBucket: "webanalyticstest-358ec.appspot.com",
    messagingSenderId: "369545131944",
    appId: "1:369545131944:web:d8fc5b14b518729830cf9d"
};

// Initialize Firebase
firebase.initializeApp(config);
var firestore = firebase.firestore();

const dbReg = firestore.collection("registrations");
const dbEvent = firestore.collection("events");

const regForm = document.getElementById("regForm");
const submittedForm = document.getElementById("submittedForm");

const submitBtn = document.getElementById("submitBtn");
const nextBtn = document.getElementById("nextBtn");

// Save registration form

function formSubmitted() {
    regForm.style.display = "none";
    submittedForm.style.display = "block";
}

function saveForm() {
    dbReg
        .add({
            firstName: regForm.firstName.value,
            lastName: regForm.lastName.value,
            email: regForm.email.value,
            phone: regForm.phone.value,
            day: regForm.day.value,
            month: regForm.month.value,
            year: regForm.year.value,
            userName: regForm.userName.value,
            password: regForm.password.value,
            regDate: firebase.firestore.Timestamp.fromDate(new Date())
        })
        .then(function(dbReg) {
            console.log("Document written with id: " + dbReg.id );
            dataLayer.push({
                'event': 'savedFormEvent',
                'eventCategory': 'savedForm',
                'eventAction': 'response',
                'eventLabel': 'register form',
                'eventValue': '1'
            });
            setTimeout(function(){ 
                window.location.href='register-success.html';
            }, 5000);
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
        });

    // Clear form
    regForm.firstName.value = '';
    regForm.lastName.value = '';
    regForm.email.value = '';
    regForm.phone.value = '';
    regForm.day.value = '';
    regForm.month.value = '';
    regForm.year.value = '';
    regForm.userName.value = '';
    regForm.password.value = '';
}

// Capture step event

function captureEvent() {
    console.log('Step: ' + currentTab + ' - Submit step: ' + eventSuccess);
    // Log event in db
    dbEvent
        .add({
            eventCategory: 'step',
            eventAction: 'submit',
            eventLabel: eventSuccess,
            eventValue: currentTab+1,
            time: firebase.firestore.Timestamp.fromDate(new Date())
        })
        .then(function(dbEvent) {
            console.log("Saved event: " + dbEvent.id);
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
        });
    // Push to analytics
    dataLayer.push({
        'event': 'stepEvent',
        'eventCategory': 'step',
        'eventAction': 'submit',
        'eventLabel': eventSuccess,
        'eventValue': currentTab+1
    });
    // console.log(dataLayer);
}

submitBtn.addEventListener('click', function(e) {
    if (!validateForm()) {
        eventSuccess = 'failed';
        console.log('Step: ' + currentTab + ' - Submit step: ' + eventSuccess);
        return false;
    }
    eventSuccess = 'success';
    captureEvent();
    formSubmitted();
    saveForm();
});